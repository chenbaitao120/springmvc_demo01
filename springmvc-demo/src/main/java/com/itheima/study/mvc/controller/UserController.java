package com.itheima.study.mvc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ：zhang
 * @date ：Created in 2019/10/22
 * @description ： 用户控制器
 * @version: 1.0
 */
@Controller
@RequestMapping("/user")
public class UserController {


    @RequestMapping("/login")
    @ResponseBody
    public String login(String username,String password){
        System.out.println(">>>>>>>u:"+username+",p:"+password);
        return "login success !";
    }
    @RequestMapping("/out")
    @ResponseBody
    public String out(String username,String password){
        System.out.println(">>>>>>>u:"+username+",p:"+password);
        return "login success !";
    }
    @RequestMapping("/two")
    @ResponseBody
    public String two(String username,String password){
        System.out.println(">>>>>>>u:"+username+",p:"+password);
        return "login success !";
    }

    @RequestMapping("/one")
    @ResponseBody
    public String one(String username,String password){
        System.out.println(">>>>>>>u:"+username+",p:"+password);
        return "login success !";
    }

}
